import { toast } from 'react-toastify';
import Router from "next/router";
export default function CartPage() {

  const onSuccess = () => {
    toast.success('Thêm vào giỏ hàng thành công', {
      className: 'GTM_AddToCartSuccess',
      onClose: () => {
        Router.push('/purchase')
      },
    });

  }


  return (
    <div>
      <button
        id="add-to-cart"
        className="mt-3 mb-3 TopSection_buttonAddToCart__yCf8T btn btn-primary"
        onClick={onSuccess}
      >
        Add to Cart Button
      </button>
    </div>
  );
}